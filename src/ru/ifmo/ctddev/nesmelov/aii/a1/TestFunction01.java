package ru.ifmo.ctddev.nesmelov.aii.a1;

public class TestFunction01 implements FitnessFunction {
    @Override
    public double calcFitness(Chromosome chromosome) {
        double x = chromosome.getGene(0);
        double y = chromosome.getGene(1);
        return x * x + y * y;
    }

    @Override
    public String getFuncName() {
        return "tf01";
    }

    @Override
    public String toString() {
        return "# TestFunction01 (Sphere -- стакан) \n" +
                "# x * x + y * y \n" +
                "# 0 0";
    }

}
