package ru.ifmo.ctddev.nesmelov.aii.a1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class SpeciesPopulation {
    // константы для общих целей
    private int populationSize; // максимально возможный размер популяции
    private int geneCount; // количество генов в хромосоме
    private double[][] ranges; // границы поиска (для генерации хромосом)
    private boolean isMinimum; // метод оптимизции [минимальное/максимальное]
    private int availableStagnationPeriods; // допустимое кол-во эпох без улучшения

    // константы для генерации потомства
    private double replicationPart; // Доля Репликации.
    private double naturalMutationPart;   // Доля Естественной мутации.
    private double crossingoverPart; // Доля Кроссинговера.
    private double replicationOffset;  // Коэффициент смещения границ интервала
    private double naturalMutationProbability; // Вероятность мутации каждого гена в %
    private int tournamentSize; // размер турнира для выбора родителя

    private FitnessFunction fitnessFunction; // фитнесс функция

    private SortedSet<Chromosome> population;
    private Chromosome[] populationArray;
    private double[] partions;
    private List<Chromosome> bestInPeriod;
    private String filePref;

    public SpeciesPopulation(int populationSize, int geneCount, double[][] ranges, final boolean isMinimum,
                             int availableStagnationPeriods,
                             double replicationPart, double naturalMutationPart, double crossingoverPart, double replicationOffset,
                             double naturalMutationProbability, int tournamentSize,
                             FitnessFunction fitnessFunction) {
        Locale.setDefault(Locale.ENGLISH);
        this.populationSize = populationSize;
        this.geneCount = geneCount;
        this.ranges = ranges;
        this.isMinimum = isMinimum;
        this.availableStagnationPeriods = availableStagnationPeriods > 3 ? availableStagnationPeriods : 3;
        this.replicationPart = replicationPart > 0 ? replicationPart : 0;
        this.naturalMutationPart = naturalMutationPart > 0 ? naturalMutationPart : 0;
        this.crossingoverPart = crossingoverPart > 0 ? crossingoverPart : 0;
        this.replicationOffset = replicationOffset;
        this.naturalMutationProbability = (naturalMutationProbability < 0) ? 0 :
                (1 < naturalMutationProbability) ? 1 : naturalMutationProbability;
        this.tournamentSize = tournamentSize;
        this.fitnessFunction = fitnessFunction;

        population = new TreeSet<Chromosome>(new ChromosomeComparator());

        // нормируем доли
        double totalPartion = this.replicationPart + this.naturalMutationPart + this.crossingoverPart;
        partions = new double[3];
        partions[0] = this.replicationPart / totalPartion;
        partions[1] = partions[0] + this.naturalMutationPart / totalPartion;
        partions[2] = partions[1] + this.crossingoverPart / totalPartion;
        bestInPeriod = new LinkedList<Chromosome>();
        filePref = fitnessFunction.getFuncName() + "/" + (int) naturalMutationPart + "-" + populationSize;
    }

    private Chromosome selectParent() {
        SortedSet<Chromosome> tournament = new TreeSet<Chromosome>(new ChromosomeComparator());
        for (int i = 0; i < tournamentSize; i++) {
            tournament.add(populationArray[ThreadLocalRandom.current().nextInt(population.size())]);
        }
        return tournament.first();
    }

    private Chromosome replication() {
        Chromosome first = selectParent();
        Chromosome second = selectParent();
        Chromosome child = new Chromosome(geneCount, fitnessFunction);
        for (int i = 0; i < geneCount; i++) {
            double low = first.getGene(i);
            double up = second.getGene(i);
            double offset = (up - low) * replicationOffset;
            if (low > up) {
//                if (low - offset > up + offset) {
                double t = low;
                low = up;
                up = t;
            }
            double newGene = low - offset;
            if (low - offset != up + offset)
                newGene = ThreadLocalRandom.current().nextDouble(low - offset, up + offset);
            child.setGene(i, newGene);
        }
        return child;
    }

    private Chromosome naturalMutation() {
        Chromosome parent = selectParent();
        Chromosome child = new Chromosome(geneCount, fitnessFunction);
        for (int i = 0; i < geneCount; i++) {
            double offset = 0; // alpha  [0..1] * d [0..intmax] * signum[-1/+1]
            if (ThreadLocalRandom.current().nextDouble() < naturalMutationProbability) {
                offset = ThreadLocalRandom.current().nextDouble() * ThreadLocalRandom.current().nextInt(1000);
                if (ThreadLocalRandom.current().nextBoolean())
                    offset *= -1;
            }
            child.setGene(i, parent.getGene(i) + offset);
        }
        return child;
    }

    private Chromosome crossingover() {
        Chromosome first = selectParent();
        Chromosome second = selectParent();
        Chromosome child = new Chromosome(geneCount, fitnessFunction);
        int breakPoint = ThreadLocalRandom.current().nextInt(geneCount);
        for (int i = 0; i < breakPoint; i++) {
            child.setGene(i, first.getGene(i));
        }
        for (int i = breakPoint; i < geneCount; i++) {
            child.setGene(i, second.getGene(i));
        }
        return child;
    }

    private void print(String filepref, int version) {
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(filepref + "/" + Integer.toString(version) + ".dat"));
            pw.println("# " + filepref + ":" + version);
            for (Chromosome t : population) {
                pw.println(t);
            }
            pw.close();
        } catch (FileNotFoundException e) {
        }
    }

    private void printMetadata(String filepref, double[] result) {
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(filepref + "/meta.dat"));
            pw.println("# populationSize: " + populationSize);
            pw.println("# geneCount: " + geneCount);
            pw.println("# isMinimum: " + isMinimum);
            pw.println("# availableStagnationPeriods: " + availableStagnationPeriods);
            pw.println(String.format("# replicationPart: %.5f", replicationPart));
            pw.println(String.format("# naturalMutationPart: %.5f", naturalMutationPart));
            pw.println(String.format("# crossingoverPart: %.5f", crossingoverPart));
            pw.println(String.format("# replicationOffset: %.5f", replicationOffset));
            pw.println(String.format("# naturalMutationProbability: %.5f", naturalMutationProbability));
            pw.println(fitnessFunction);
            pw.println("# tournamentSize: " + tournamentSize);
            pw.print("# best ");
            for (int i = 0; i < result.length; i++) {
                pw.printf("%.7f  ", result[i]);
            }
            pw.println();
            pw.println("# ranges   " + Arrays.deepToString(ranges));
            for (int i = 0; i < bestInPeriod.size(); i++) {
                Chromosome t = bestInPeriod.get(i);
                pw.println(i + " " + t.getGene(0) + " " + t.getGene(1) + " " + t.calcFitness());
            }
            pw.close();
        } catch (FileNotFoundException e) {
        }
    }

    public double[] execute() {
        int periodTotal = 0;
        int periodStagnation = 0;
        Chromosome best = null;
//        String filePref = Long.toString(new Date().getTime());
        new File(filePref).mkdirs();

        // заполняем протопопуляцию
        for (int i = 0; i < populationSize; i++) {
            Chromosome t = new Chromosome(geneCount, fitnessFunction);
            for (int gene = 0; gene < geneCount; gene++) {
                t.setGene(gene, ranges[gene][0] != ranges[gene][1] ?
                        ThreadLocalRandom.current().nextDouble(ranges[gene][0], ranges[gene][1]) : ranges[gene][0]);
            }
            t.calcFitness();
            population.add(t);
        }
        print(filePref, periodTotal);
        best = population.first();

        while (periodStagnation < availableStagnationPeriods) {
            populationArray = population.toArray(new Chromosome[population.size()]);
            SortedSet<Chromosome> offsprings = new TreeSet<Chromosome>(new ChromosomeComparator());
            for (int i = 0; i < populationSize; i++) {
                double operator = ThreadLocalRandom.current().nextDouble();
                Chromosome child;
                if (operator < partions[0])
                    child = replication();
                else if (operator < partions[1])
                    child = naturalMutation();
                else
                    child = crossingover();
                child.calcFitness();
                offsprings.add(child);
            }
            Chromosome bestOffspring = offsprings.first();
            if (new ChromosomeComparator().compare(best, bestOffspring) > 0) {
                best = bestOffspring;
                periodStagnation = -1;
            }
            population = offsprings;
            population.add(best);
            periodTotal++;
            periodStagnation++;
            print(filePref, periodTotal);
//            System.out.format("%.5f %.5f %.5f%n", best.getGene(0), best.getGene(1), best.calcFitness());
            bestInPeriod.add(best);
            if (periodTotal > 50000)
                break;
        }
        double[] result = new double[geneCount];
        for (int i = 0; i < geneCount; i++) {
            result[i] = best.getGene(i);
        }
        printMetadata(filePref, result);
        return result;
    }

    class ChromosomeComparator implements Comparator<Chromosome> {
        @Override
        public int compare(Chromosome o1, Chromosome o2) {
            int t = Math.abs(o1.calcFitness() - o2.calcFitness()) < 0.00000001 ? 0
                    : Double.compare(o1.calcFitness(), o2.calcFitness());
            return isMinimum ? t : -t;
        }
    }
}
