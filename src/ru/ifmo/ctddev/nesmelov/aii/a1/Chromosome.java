package ru.ifmo.ctddev.nesmelov.aii.a1;

import java.util.Arrays;

public class Chromosome {
    private double[] genes;
    Double fitness;
    FitnessFunction fitnessFunction;

    public Chromosome(int size, FitnessFunction fitnessFunction) {
        genes = new double[size];
        this.fitnessFunction = fitnessFunction;
    }

    private void ensureCapacity(int index) {
        if (index < 0 || genes.length <= index)
            throw new IndexOutOfBoundsException("requested " + index + " but only " + genes.length + " is available");
    }

    public double calcFitness() {
        if (fitness == null)
            fitness = fitnessFunction.calcFitness(this);
        return fitness;
    }

    public double getGene(int index) {
        ensureCapacity(index);
        return genes[index];
    }

    public void setGene(int index, double value) {
        ensureCapacity(index);
        genes[index] = value;
        fitness = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chromosome that = (Chromosome) o;

        if (!Arrays.equals(genes, that.genes)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(genes);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < genes.length; i++) {
            sb.append(genes[i]).append(" ");
        }
        sb.append(calcFitness());
        return sb.toString();
    }
}
