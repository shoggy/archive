package ru.ifmo.ctddev.nesmelov.aii.a1;

public class TestFunction12 implements FitnessFunction {
    @Override
    public double calcFitness(Chromosome chromosome) {
        double x = chromosome.getGene(0);
        double y = chromosome.getGene(1);
        return (1 + (x + y + 1) * (x + y + 1) * (19 - 14 * x + 3 * x * x - 14 * y + 6 * x * y + 3 * y * y)) *
                (30 + (2 * x - 3 * y) * (2 * x - 3 * y) * (18 - 32 * x + 12 * x * x + 48 * y - 36 * x * y + 27 * y * y));
    }

    @Override
    public String getFuncName() {
        return "tf12";
    }

    @Override
    public String toString() {
        return "# TestFunction12 (Goldstein–Price)\n" +
                "# (1+(x+y+1)**2 *(19-14*x+3*x**2-14*y+6*x*y+3*y**2)) * (30+(2*x-3*y)**2*(18-32*x+12*x**2+48*y-36*x*y+27*y**2)) \n" +
                "# 0 -1";
    }
}
