package ru.ifmo.ctddev.nesmelov.aii.a1;

public class TestFunction13 implements FitnessFunction {
    /* Rosenbrok
    * 1 1*/
    @Override
    public double calcFitness(Chromosome chromosome) {
        double x = chromosome.getGene(0);
        double y = chromosome.getGene(1);
        return (1 - x) * (1 - x) + 100 * (y - x * x) * (y - x * x);
    }

    @Override
    public String getFuncName() {
        return "tf13";
    }

    @Override
    public String toString() {
        return "# TestFunction13 (Rosenbrock) \n" +
                "# (1 - x) ** 2 + 100 * (y - x * x) ** 2 \n" +
                "# 1 1";
    }
}
