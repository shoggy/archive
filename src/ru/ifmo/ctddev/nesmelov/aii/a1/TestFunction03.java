package ru.ifmo.ctddev.nesmelov.aii.a1;

public class TestFunction03 implements FitnessFunction {
    @Override
    public double calcFitness(Chromosome chromosome) {
        double x = chromosome.getGene(0);
        double y = chromosome.getGene(1);
        return (x + 2 * y - 7) * (x + 2 * y - 7) + (2 * x + y - 5) * (2 * x + y - 5);
    }

    @Override
    public String getFuncName() {
        return "tf03";
    }

    @Override
    public String toString() {
        return "# TestFunction03 (Booth function) \n" +
                "# (x + 2 * y - 7) ** 2 + (2 * x + y - 5) ** 2 \n" +
                "# 1 3";
    }

}
