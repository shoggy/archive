package ru.ifmo.ctddev.nesmelov.aii.a1;

public interface FitnessFunction {
    public double calcFitness(Chromosome chromosome);
    public String getFuncName();
}
