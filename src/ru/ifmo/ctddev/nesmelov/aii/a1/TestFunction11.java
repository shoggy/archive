package ru.ifmo.ctddev.nesmelov.aii.a1;

import static java.lang.Math.*;

public class TestFunction11 implements FitnessFunction {
    @Override
    public double calcFitness(Chromosome chromosome) {
        double x = chromosome.getGene(0);
        double y = chromosome.getGene(1);
        return -20 * exp(-0.2 * sqrt(0.5 * (x * x + y * y))) - exp(0.5 * (cos(2 * PI * x) + cos(2 * PI * y))) + E + 20;
    }

    @Override
    public String getFuncName() {
        return "tf11";
    }

    @Override
    public String toString() {
        return "# TestFunction11 (Ackley's)\n" +
                "# -20 * exp(-0.2 * sqrt(0.5 * (x * x + y * y))) - exp(0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + exp(1) + 20 \n" +
                "# 0 0";
    }
}
