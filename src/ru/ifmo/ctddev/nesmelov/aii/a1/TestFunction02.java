package ru.ifmo.ctddev.nesmelov.aii.a1;

public class TestFunction02 implements FitnessFunction {
    @Override
    public double calcFitness(Chromosome chromosome) {
        double x = chromosome.getGene(0);
        double y = chromosome.getGene(1);
        return (x - 1) * (x - 1) + (y - 1) * (y - 1) - x * y;
    }

    @Override
    public String getFuncName() {
        return "tf02";
    }

    @Override
    public String toString() {
        return "# TestFunction02 (Trid function) \n" +
                "# (x - 1) ** 2 + (y - 1) ** 2 - x * y \n" +
                "# 2 2";
    }

}
