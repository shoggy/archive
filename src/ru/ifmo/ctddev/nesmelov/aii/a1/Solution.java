package ru.ifmo.ctddev.nesmelov.aii.a1;

public class Solution {
    public static void main(String[] args) {
//        double[][] goodRange = {{-3, 3}, {-3, 3}};
//        double[][] goodRange = {{-100, 100}, {-100, 100}};
//        double[][] goodRange = {{-1000, 1000}, {-1000, 1000}};
//        double[][] goodRange = {{-10000, 10000}, {-10000, 10000}};
//        double[][] goodRange = {{-100, -2}, {-100, -2}};
//        double[][] goodRange = {{-512, 512}, {-512, 512}};
        double[][] goodRange = {{200, 500}, {200, 500}};

//        SpeciesPopulation test = new SpeciesPopulation(100, 2, range, true, 13,
//                90, 10, 0, 0.1, 0.01, 5,
//                new TestFunction12());


//        SpeciesPopulation test = new SpeciesPopulation(100, 2, range, true, 13,
//                90, 10, 0, 0.1, 0.01, 5,
//                new TestFunction11());


//        SpeciesPopulation test = new SpeciesPopulation(100, 2, range, true, 13,
//                90, 10, 0, 0.1, 0.01, 5,
//                new TestFunction03());


//        SpeciesPopulation test = new SpeciesPopulation(100, 2, range, true, 13,
//        90, 10, 0, 0.1, 0.01, 5,
//        new TestFunction13());
//
//        SpeciesPopulation test = new SpeciesPopulation(100, 2, goodRange, true, 13,
//        90, 10, 0, 0.1, 0.5, 5,
//        new TestFunction01());
//        test.execute();


        int[] mut = {0, 25, 50, 75};
        int[] pop = {10, 100, 1000};
        FitnessFunction[] ff = {new TestFunction01(), new TestFunction02(), new TestFunction03(),
                new TestFunction11(), new TestFunction12(), new TestFunction13()};
//        FitnessFunction[] ff = {new TestFunction11()};
        for (FitnessFunction f : ff) {
            for (int i : mut) {
                for (int j : pop) {
                    SpeciesPopulation test = new SpeciesPopulation(j, 2, goodRange, true, 13,
                            100 - i, i, 0, 0.1, 1, 5,
                            f);
                    test.execute();
                }
            }
        }


//        double[] result = test.execute();
//        System.out.println(Arrays.toString(result));
    }
}
